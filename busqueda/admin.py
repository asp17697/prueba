from django.contrib import admin
from busqueda.models import *

admin.site.register(Estado)
admin.site.register(Municipio)
admin.site.register(Colonia)
