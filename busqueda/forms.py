from django import forms

class formulario_busqueda(forms.Form):
    b_estado = forms.CharField(label='Estado', max_length=25, required=False)
    b_municipio = forms.CharField(label='Municipio', max_length=50, required=False)
    b_colonia = forms.CharField(label='Colonia', max_length=25, required=False)
    b_cp = forms.CharField(label='CP', max_length=5, required=False)