from django.db import models

class Estado(models.Model):
    d_estado = models.CharField(max_length=25)
    c_estado = models.CharField(max_length=2)

    def __str__(self):
        template = '{0.d_estado} {0.c_estado}'
        return template.format(self)

class Municipio(models.Model):
    D_mnpio = models.CharField(max_length=50)
    c_mnpio = models.CharField(max_length=6)
    c_estado = models.ForeignKey(Estado,  null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        template = '{0.c_mnpio} {0.D_mnpio} {0.c_estado}'
        return template.format(self)

class Colonia(models.Model):
    d_asenta = models.CharField(max_length=25)
    id_asenta_cpcons = models.CharField(max_length=6)
    d_CP = models.CharField(max_length=5)
    c_mnpio = models.ForeignKey(Municipio,  null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        template = '{0.d_CP} {0.d_asenta} {0.c_mnpio}'
        return template.format(self)
