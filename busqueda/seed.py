import xlrd
import sqlite3
from sqlite3 import connect
from sqlite3 import Error
from models import Estado
from models import Municipio
from models import Colonia

libro = xlrd.open_workbook("CPdescarga.xls")
hojas_estado = [None]*32
hojas_estado[0] = libro.sheet_by_name("Aguascalientes")
hojas_estado[1] = libro.sheet_by_name("Baja_California")
hojas_estado[2] = libro.sheet_by_name("Baja_California_Sur")
hojas_estado[3] = libro.sheet_by_name("Campeche")
hojas_estado[4] = libro.sheet_by_name("Coahuila_de_Zaragoza")
hojas_estado[5] = libro.sheet_by_name("Colima")
hojas_estado[6] = libro.sheet_by_name("Chiapas")
hojas_estado[7] = libro.sheet_by_name("Chihuahua")
hojas_estado[8] = libro.sheet_by_name("Distrito_Federal")
hojas_estado[9] = libro.sheet_by_name("Durango")
hojas_estado[10] = libro.sheet_by_name("Guanajuato")
hojas_estado[11] = libro.sheet_by_name("Guerrero")
hojas_estado[12] = libro.sheet_by_name("Hidalgo")
hojas_estado[13] = libro.sheet_by_name("Jalisco")
hojas_estado[14] = libro.sheet_by_name("México")
hojas_estado[15] = libro.sheet_by_name("Michoacán_de_Ocampo")
hojas_estado[16] = libro.sheet_by_name("Morelos")
hojas_estado[17] = libro.sheet_by_name("Nayarit")
hojas_estado[18] = libro.sheet_by_name("Nuevo_León")
hojas_estado[19] = libro.sheet_by_name("Oaxaca")
hojas_estado[20] = libro.sheet_by_name("Puebla")
hojas_estado[21] = libro.sheet_by_name("Querétaro")
hojas_estado[22] = libro.sheet_by_name("Quintana_Roo")
hojas_estado[23] = libro.sheet_by_name("San_Luis_Potosí")
hojas_estado[24] = libro.sheet_by_name("Sinaloa")
hojas_estado[25] = libro.sheet_by_name("Sonora")
hojas_estado[26] = libro.sheet_by_name("Tabasco")
hojas_estado[27] = libro.sheet_by_name("Tamaulipas")
hojas_estado[28] = libro.sheet_by_name("Tlaxcala")
hojas_estado[29] = libro.sheet_by_name("Veracruz_de_Ignacio_de_la_Llave")
hojas_estado[30] = libro.sheet_by_name("Yucatán")
hojas_estado[31] = libro.sheet_by_name("Zacatecas")

con = 0
id_mun_com = "Null"

for m in range(len(hojas_estado)):
    for r in range(1, hojas_estado[m].nrows):
        print(con)
        con = con+1

        id_estado = hojas_estado[m].cell(r,7).value
        estado = hojas_estado[m].cell(r,4).value
        if con==0:
            Estado.objects.create(d_estado = id_estado, c_estado=estado)

        colonia = hojas_estado[m].cell(r,1).value
        id_colonia = hojas_estado[m].cell(r,12).value
        cp = hojas_estado[m].cell(r,6).value
        id_municipio = hojas_estado[m].cell(r,11).value

        if id_municipio != id_mun_com:
            municipio= hojas_estado[m].cell(r,3)
            Municipio.objects.create(D_mnpio=municipio, c_mnpio=id_municipio, c_estado_id=id_estado)

        id_mun_com = id_municipio
        Colonia.objects.create(d_asenta=colonia, id_asenta_cpcons=id_colonia, d_CP=cp, c_mnpio_id=id_municipio)
