from django.shortcuts import render
from django.http import HttpResponse
from .forms import formulario_busqueda
from django.db.models import Q
from .models import Estado,Municipio,Colonia

def busqueda(request):
    form = formulario_busqueda()
    
    if request.method == 'POST':
        form=formulario_busqueda(request.POST)
        if form.is_valid():
            Estado_ingresado = form.cleaned_data.get('b_estado', 'vacio')
            Municipio_ingresado = form.cleaned_data.get('b_municipio', 'vacio')
            Colonia_ingresado = form.cleaned_data.get('b_colonia', 'vacio')
            cp_ingresado = form.cleaned_data.get('b_cp', 'vacio')
            
            if (cp_ingresado != '' or Colonia_ingresado!=''):
                Colonia_encontrada = Colonia.objects.filter(Q(d_CP=cp_ingresado) | Q(d_asenta=Colonia_ingresado))
                return HttpResponse(Colonia_encontrada)
    return render(request, 'busqueda/main.html',{'form':form})
    #return HttpResponse("Hello, world. You're at the polls index.")
    
def buscador(request):
    mensaje = "Información ingresada"
    return HttpResponse(mensaje)